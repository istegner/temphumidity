﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TempHumidityGUI
{
    public partial class Form1 : Form
    {
        private const int port = 4210;

        private bool error = false;

        DateTime timeLastPacket;

        UdpClient listener = new UdpClient(port);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MainLoopAsync();
        }

        private async Task MainLoopAsync()
        {
            Application.DoEvents();
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
            byte[] recieveByteArray;

            try
            {
                while (true)
                {
                    Application.DoEvents();
                    UdpReceiveResult result = await listener.ReceiveAsync();
                    DateTime localDT = System.DateTime.Now;
                    if (localDT.ToString() != timeLastPacket.ToString())
                    {
                        recieveByteArray = result.Buffer;
                        float t = BitConverter.ToSingle(recieveByteArray, 0);
                        float h = BitConverter.ToSingle(recieveByteArray, 4);
                        float hic = BitConverter.ToSingle(recieveByteArray, 8);
                        float pres = BitConverter.ToSingle(recieveByteArray, 12);
                        timeLastPacket = localDT;

                        DateTime_TB.Text = localDT.ToString();
                        Temperature_TB.Text = t.ToString("#.##");
                        Humidity_TB.Text = h.ToString("#.##");
                        HI_TB.Text = hic.ToString("#.##");
                        AP_Tb.Text = pres.ToString("#.##");
                    }
                }
            }
            catch (Exception e)
            {
                error = true;
                throw;
            }
        }
    }
}
