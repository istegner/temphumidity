﻿namespace TempHumidityGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DateTime_TB = new System.Windows.Forms.TextBox();
            this.Temperature_TB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Humidity_TB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.HI_TB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AP_Tb = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "As of";
            // 
            // DateTime_TB
            // 
            this.DateTime_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTime_TB.Location = new System.Drawing.Point(237, 12);
            this.DateTime_TB.Name = "DateTime_TB";
            this.DateTime_TB.ReadOnly = true;
            this.DateTime_TB.Size = new System.Drawing.Size(323, 36);
            this.DateTime_TB.TabIndex = 1;
            this.DateTime_TB.Text = "31/12/2020 12:59:59 AM";
            // 
            // Temperature_TB
            // 
            this.Temperature_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Temperature_TB.Location = new System.Drawing.Point(237, 54);
            this.Temperature_TB.Name = "Temperature_TB";
            this.Temperature_TB.ReadOnly = true;
            this.Temperature_TB.Size = new System.Drawing.Size(323, 36);
            this.Temperature_TB.TabIndex = 3;
            this.Temperature_TB.Text = "99.99";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 29);
            this.label2.TabIndex = 2;
            this.label2.Text = "Temperature (°c)";
            // 
            // Humidity_TB
            // 
            this.Humidity_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Humidity_TB.Location = new System.Drawing.Point(237, 96);
            this.Humidity_TB.Name = "Humidity_TB";
            this.Humidity_TB.ReadOnly = true;
            this.Humidity_TB.Size = new System.Drawing.Size(323, 36);
            this.Humidity_TB.TabIndex = 5;
            this.Humidity_TB.Text = "99.99";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Humidity (%)";
            // 
            // HI_TB
            // 
            this.HI_TB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HI_TB.Location = new System.Drawing.Point(237, 138);
            this.HI_TB.Name = "HI_TB";
            this.HI_TB.ReadOnly = true;
            this.HI_TB.Size = new System.Drawing.Size(323, 36);
            this.HI_TB.TabIndex = 7;
            this.HI_TB.Text = "99.99";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(179, 29);
            this.label4.TabIndex = 6;
            this.label4.Text = "Heat Index (°c)";
            // 
            // AP_Tb
            // 
            this.AP_Tb.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AP_Tb.Location = new System.Drawing.Point(237, 180);
            this.AP_Tb.Name = "AP_Tb";
            this.AP_Tb.ReadOnly = true;
            this.AP_Tb.Size = new System.Drawing.Size(323, 36);
            this.AP_Tb.TabIndex = 9;
            this.AP_Tb.Text = "99.99";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 29);
            this.label5.TabIndex = 8;
            this.label5.Text = "Air Pressure (kPa)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 233);
            this.Controls.Add(this.AP_Tb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.HI_TB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Humidity_TB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Temperature_TB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DateTime_TB);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TempHumidity";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox DateTime_TB;
        private System.Windows.Forms.TextBox Temperature_TB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Humidity_TB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox HI_TB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox AP_Tb;
        private System.Windows.Forms.Label label5;
    }
}

