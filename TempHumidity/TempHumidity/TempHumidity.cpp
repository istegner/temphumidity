#include <iostream>
//Set up Winsock
#include <WinSock2.h>
#include <MSWSock.h>
#include <WS2tcpip.h>
#include <iostream>
#include <fstream>
#include <ctime>

#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable : 4996)

std::string ipAddress;
int port;
std::string name;

SOCKET CreateSocket();
void Bind(sockaddr_in _ipAddress, SOCKET socket);
int RecievePacket(SOCKET socket);

template<typename T>
int SendPacket(T toSend, SOCKET socket, sockaddr_in address)
{
	int result;
	result = sendto(socket, (const char*)&toSend, sizeof(T), 0, (SOCKADDR*)&address, sizeof(address));
	if (result == SOCKET_ERROR)
	{
		std::cout << "sendto() failed: Error " << WSAGetLastError() << std::endl;
		return result;
	}
	return 0;
}

int main(int argc, char* argv[])
{
	if (argc == 3)
	{
		ipAddress = argv[1];
		port = atoi(argv[2]);
	}
	else
	{
		/*ipAddress = "127.0.0.1";
		port = 1300;*/
		std::cout << "IPAddress: ";
		std::cin >> ipAddress;
		std::cout << "Port: ";
		std::cin >> port;
		//std::cout << "Client name: ";
		//std::cin >> name;
	}


	//Init Winsock
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		std::cout << "Failed to initialise winsock" << std::endl;
		return 1;
	}

	//Make an address for listening
	sockaddr_in listenAddress;
	//IPv4 address family
	listenAddress.sin_family = AF_INET;
	listenAddress.sin_addr.s_addr = INADDR_ANY;
	//Port to listen to
	listenAddress.sin_port = htons(port);

	//Create a socket
	SOCKET mySocket = CreateSocket();
	if (mySocket == NULL)
	{
		std::cout << "Failed to open socket" << std::endl;
		return 1;
	}

	//Send packet
	//const char* message = "Hello";
	//SendPacket(message, mySocket, address);

	//Bind to a socket
	Bind(listenAddress, mySocket);

	//Recieve packet
	while (true)
	{
		RecievePacket(mySocket);
	}

	//Cleanup
	WSACleanup();

	return 0;
}

SOCKET CreateSocket()
{
	SOCKET mySocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (mySocket == SOCKET_ERROR)
	{
		std::cout << "Error opening socket" << std::endl;
		return NULL;
	}

	//Fix for socket breaking if receiver crashed
	DWORD dwBytesReturned = 0;
	BOOL bNewBehaviour = FALSE;
	WSAIoctl(mySocket,
		SIO_UDP_CONNRESET,
		&bNewBehaviour,
		sizeof(bNewBehaviour),
		NULL, 0,
		&dwBytesReturned,
		NULL, NULL);

	//Allow broadcasting
	BOOL bOptVal = TRUE;
	setsockopt(mySocket,
		SOL_SOCKET,
		SO_BROADCAST,
		(const char*)&bOptVal,
		sizeof(BOOL));

	return mySocket;
}

void Bind(sockaddr_in _ipAddress, SOCKET socket)
{
	int result;
	result = bind(socket, (SOCKADDR*)&_ipAddress, sizeof(_ipAddress));
	if (result == SOCKET_ERROR)
	{
		std::cout << "Bind() failed: Error " << WSAGetLastError() << std::endl;
	}
}

std::string convertToString(char* a, int size)
{
	int i;
	std::string s = "";
	for (i = 0; i < size; i++) {
		s = s + a[i];
	}
	return s;
}

/// <summary>
/// Listen for packets on a socket
/// </summary>
/// <param name="socket">The socket to listen on</param>
int RecievePacket(SOCKET socket)
{
	std::ofstream logFile;
	int waiting;
	do
	{
		fd_set checksockets;
		checksockets.fd_count = 1;
		checksockets.fd_array[0] = socket;

		struct timeval t;
		t.tv_sec = 0;
		t.tv_usec = 0;

		waiting = select(NULL, &checksockets, NULL, NULL, &t);

		if (waiting > 0)
		{
			if (!logFile.is_open())
				logFile.open("tempHumLog.txt", std::ios::app);
			// current date/time based on current system
			time_t now = time(0);
			// convert now to string form
			char* dt = ctime(&now);

			char buffer[10000];
			sockaddr_in from;
			int fromLength = sizeof(from);
			int size = recvfrom(socket, buffer, 10000, 0, (SOCKADDR*)&from, &fromLength);
			auto message = dt + convertToString(buffer, sizeof(buffer));
			char cstr[sizeof(message)];
			strcpy(cstr, message.c_str());
			std::cout << cstr << std::endl;
			logFile.write(cstr, message.size());
		}
	} while (waiting);

	if (logFile.is_open())
		logFile.close();
	return 0;
}
