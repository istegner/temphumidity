#pragma pack(push,1)
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "AndroidAP";
const char* password = "111222333";

WiFiUDP udp;
unsigned int port = 1379;
char incomingPacket[255];
IPAddress ipAddress(255, 255, 255, 255);

class Packet
{
public:
	byte packetType;
	int packetNum;

	enum packetTypes
	{
		ANNOUNCE,
		ACKNOWLEDGE,
		UPDATE,
		SHUTDOWN,
		SERVERFULL
	};
};

class Pkt_Announce : public Packet
{
public:
	byte msg[5] = { 'H', 'e', 'l', 'l', 'o' };

	Pkt_Announce()
	{
		packetType = packetTypes::ANNOUNCE;
	}
};

class Pkt_Ack : public Packet
{
public:
	byte r;
	byte g;
	byte b;
	short axisMaxVal;

	Pkt_Ack()
	{
		packetType = packetTypes::ACKNOWLEDGE;
	}
};

class Pkt_Update() : public Packet
{

}

template<typename T>
int SendPacket(T toSend)
{
	char* message = (char*)&toSend;
	Serial.println("Sending:");
	Serial.println(sizeof(toSend));
	udp.beginPacket(ipAddress, port);
	udp.write(message, sizeof(toSend));
	udp.endPacket();
}

void setup()
{
	Serial.begin(9600);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Serial.print(".");
	}


	Serial.println("");
	Serial.println("WiFi connected");
	// Print the IP address
	Serial.println(WiFi.localIP());
	Serial.println(port);
	udp.begin(port);

	Pkt_Announce announcePkt;
	SendPacket(announcePkt);
}

void loop()
{
	int waiting;
	do
	{
		waiting = udp.parsePacket();
		if (waiting > 0)
		{
			char buffer[10000];
			udp.read(buffer, 255);
			Packet* p;
			p = (Packet*)buffer;
			switch (p->packetType)
			{
			case Packet::ACKNOWLEDGE:
			{
				Pkt_Ack* pa = (Pkt_Ack*)p;
				Serial.println(pa->axisMaxVal);
				break;
			}
			default:
			{
				Serial.println("Packet not recognised");
				Serial.println(p->packetType);
			}
			}
		}
	} while (waiting);
}