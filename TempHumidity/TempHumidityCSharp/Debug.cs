﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TempHumidityCSharp
{
    class Debug
    {
        public static void Log(float _temp, float _hum, float _heatI, int _loc, float _pressure)
        {
            DateTime localDT = DateTime.Now;
            string toWrite = _loc + "," + localDT.ToString() + "," + _temp + "," + _hum + "," + _heatI + "," + _pressure;
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tempHumLog.csv");
            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Location,Date-Time,Temperature (°c),Humidity (%),Heat Index (°c),Pressure (kPa)");
                    sw.WriteLine(toWrite);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(toWrite);
                }
            }
            Log(toWrite);
        }

        public static void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
