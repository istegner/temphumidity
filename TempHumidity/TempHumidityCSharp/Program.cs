﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using TempHumidityCSharp;

class Program
{
    public bool shutdown = false;
    private const int port = 4210; //The port

    UdpClient listener = new UdpClient(port); //What to listen on for packets

    //Start the program
    static int Main()
    {
        Program p = new Program();
        p.MainLoop();
        return 0;
    }

    int MainLoop()
    {
        Debug.Log("Starting...");

        IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
        byte[] recieveByteArray;

        try
        {
            //While the server is running
            while (!shutdown)
            {
                Debug.Log("Waiting for packet");
                recieveByteArray = listener.Receive(ref endPoint);
                Debug.Log("Packet recieved from " + endPoint.Address.ToString()); //Print what IP tha packet came from
                float t = BitConverter.ToSingle(recieveByteArray, 0);
                float h = BitConverter.ToSingle(recieveByteArray, 4);
                float hic = BitConverter.ToSingle(recieveByteArray, 8);
                float pres = BitConverter.ToSingle(recieveByteArray, 12);
                int loc = BitConverter.ToUInt16(recieveByteArray, 16);
                Debug.Log(t, h, hic, loc, pres);
            }
        }
        //Something went wrong
        catch (Exception e)
        {
            Debug.Log("Error: " + e.ToString());
        }
        return 0;
    }
}
