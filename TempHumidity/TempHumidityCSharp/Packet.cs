﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace TempHumidityCSharp
{
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public abstract class Packet
    {
        [FieldOffset(0)] public float temp;
        [FieldOffset(4)] public float hum;
        [FieldOffset(8)] public float heatI;
        [FieldOffset(12)] public float pressure;
        [FieldOffset(16)] public byte location;
    }
}
