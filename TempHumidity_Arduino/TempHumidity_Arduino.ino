// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#include "DHT.h"
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Wire.h>
#include "SparkFunBME280.h"

#define DHTPIN 2    // what digital pin we're connected to

#define DHTTYPE DHT11   // DHT 11

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

/*
  Hardware connections:
  BME280 -> Arduino
  GND -> GND
  3.3 -> 3.3
  SDI -> D2
  SCK -> D1
*/

BME280 bme;

const char* ssid1 = "Fast1";
const char* password1 = "Rumboligan12228";
const char* ssid2 = "OPTUS_2CC2B6";
const char* password2 = "yowiethuds96641";

int loc = 1; //0 - Bedroom, 1 = Other

WiFiUDP udp;
unsigned int localUdpPort = 4210;
IPAddress IP_Address(106, 70, 253, 10);
IPAddress IP_Broadcast(255, 255, 255, 255);

class Packet
{
  public:
    float temp;
    float hum;
    float heatI;
    float pressure;
    int location;

    Packet()
    {
      location = loc;
    }
};

template<typename T>
int SendPacket(T toSend)
{
  char* message = (char*)&toSend;
  //  Serial.println("Sending to server");
  //  udp.beginPacket(IP_Address, localUdpPort);
  //  udp.write(message, sizeof(toSend));
  //  udp.endPacket();

  Serial.println("Broadcasting");
  udp.beginPacket(IP_Broadcast, localUdpPort);
  udp.write(message, sizeof(toSend));
  udp.endPacket();
  Serial.println("Sent");
}

void ConnectInternet()
{
  WiFi.begin(ssid1, password1);
  int tryNum = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    tryNum++;
    if (tryNum >= 60)
    {
      Serial.println("Failed to connect to ssid1");
      break;
    }
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    loc = 0;
    return;
  }

  WiFi.begin(ssid2, password2);
  tryNum = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
    tryNum++;
    if (tryNum >= 60)
    {
      Serial.println("Failed to connect to ssid2");
      break;
    }
  }
  loc = 1;
}

void setup() {
  Serial.begin(9600);

  ConnectInternet();

  Serial.println("");
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());
  Serial.println(localUdpPort);

  dht.begin();
  udp.begin(localUdpPort);
  Wire.begin();
  bme.setI2CAddress(0x76);
  if (bme.beginI2C() == false) while(1);
  
  delay(5000);

  Serial.println("");
  Serial.println("Temperature (°c),Humidity (%),Heat Index (°c),Pressure (kPa)");
}

void loop() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  Serial.println("Reading data from sensor");
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  float pres = bme.readFloatPressure() / 1000.00;

  Serial.println((String)t + "," + (String)h + "," + (String)hic + "," + (String)pres);

  //Send packet
  Packet packet;
  packet.temp = t;
  packet.hum = h;
  packet.heatI = hic;
  packet.pressure = pres;
  SendPacket(packet);
  delay(600000);
}
